# Analysis of gene expression similarity patterns #
  
### Liisi Henno, Mihkel Vaher 
  
## Introduction ##
  
In the mouse embryo, the transition from blastocyst to gastrula is an elaborate process involving a complex series of molecular and cellular events. To date, many regulatory genes coordinating mammalian development have been identified. 
We will apply bioinformatic approach to identify gene expression patterns that imply a role in embryogenesis or some other biological process. A selection of these findings can be used in a more comprehensive study.

## Data ##
  
The data under analysis is obtained from RNA expression profiling of mouse embryonic stem cell line CGR8 during the first 10 days of differentiation into embryoid bodies. The germ-line competent cell line CGR8 was established from the inner cell mass of a 3.5 day male preimplantation mouse embryo (Mus musculus, strain 129). These pluripotent cells retain the ability to participate in normal embryonic development. The dataset consisted of 45101 genes.
  
## Methods ##
  
To separate the genes we applied hierarchical and TADPole clustering algorithms in R software, using ‘hclust’ and ‘dtwclust’ packages respectively. Reading the initial data was done using the RNetCDF and visualization with ggplot2 package. In both cases a subselection of 10 000 genes was analysed.
  
## Hierarchical clustering ##
  
For hierarchical clustering ‘complete link’ was used as a distance measure and cutree = 10, resulting in 10 clusters. To visualize each cluster with one time series, the time points were averaged. The cluster names are the most abundant word in the cluster’s description for every gene (gene name and description was obtained using the gProfileR package). The number following shows the count of the word and ‘N’ the total size of the cluster.
  
## TADPole clustering algorithm ##
   
TADPole clustering adopts a relatively new clustering framework (2015) and adapts it to time series clustering with DTW. The algorithm first uses the DTW upper and lower bounds (Euclidean and LB_Keogh respectively) to find series with many close neighbors (in DTW space). Anything below the cutoff distance is considered a neighbor. Aided with this information, the algorithm then tries to prune as many DTW calculations as possible in order to accelerate the clustering procedure. 
   
## Results and discussion ##
### Hierarchical clustering ###
  
Comparing the 10 clusters we can see that many of the genes remain expressed at a constant rate throughout the measurement. The most significant change is in the cluster ‘1collagen’ with a significant rise in the expression. Collagen is the main structural protein in the animal body so this result is expected. The cluster ‘1homeobox’ has a jump on the 9th day which then gradually decreases. Genes with the homeobox sequence have been found to play a role in the determination of body axes. Other changes are more subtle: increase in different receptors and a decrease in the ‘dehydrogenase’ cluster. As the embryo develops more receptors are needed as the organism gets more complex. The decrease of ‘dehydrogenase’ might be due to the consumption and running out of the energy rich compounds found in the egg cell.
These findings have to be confirmed by literature or with experiments to make any further conclusions. For example when are the homeobox genes expressed.
  
### Dynamic time warp clustering ###
  
In principle the dynamic time warp clustering is more suitable to the task, because in case of the hierarchical clustering each time point is taken separately whereas in reality it is continuous data. The resulting 20 clusters (cutoff distance was set to 1.5) visualize different genes’ expression patterns. Most of the genes are in clusters with constant expression levels. The 5th cluster gene expression levels increase and fluctuate in time. These genes might have a role in mouse embryogenesis since the expression levels increase quite significantly. This hypothesis is supported by information obtained from analysing the clustered gene’s biological roles (Mouse Gene Expression Database, http://www.informatics.jax.org/). If we take a random gene from cluster 5: Cxcl12, a chemokine protein, is expressed in many tissues and cell types. Cxcl12 directs the migration of hematopoietic cells from fetal liver to bone marrow and the formation of large blood vessels. We can also find other chemokine family proteins from cluster 5, which is expected, as they have similar roles in a developing mouse embryo (inducing directed chemotaxis in nearby responsive cells).
However, the possible interaction of the genes and dependencies obtained from clustering need to be further confirmed by literature to make suggestions for novel gene interaction mechanisms.

  
This experiment is part of the FunGenES project (FunGenES - Functional Genomics in Embryonic Stem Cells partially funded by the 6th Framework Programme of the European Union, http://www.fungenes.org). The experiment was conducted at University of Cologne, Cologne, Germany.